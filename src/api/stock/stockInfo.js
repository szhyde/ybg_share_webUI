import request from '@/utils/request'
// 个股分页列表
export function pageList(data) {
  return request({
    url: '/api/shareadmin/share/stock/page',
    method: 'post',
    data
  })
}

// 禁用/启用

// 同步所有股票列表
export function syncAllStock() {
  return request({
    url: '/api/shareadmin/share/stock/syncAllStock',
    method: 'post'
  })
}
// 更新所有个股列表信息
export function updateAllStockInfo() {
  return request({
    url: '/api/shareadmin/share/stock/updateAllStockInfo',
    method: 'post'
  })
}
// 启用或者禁用个股
export function banStock(stockId) {
  return request({
    url: '/api/shareadmin/share/stock/banStock',
    method: 'post',
    params: { stockId }
  })
}

